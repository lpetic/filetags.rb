#!/usr/bin/env ruby
require 'ffi-xattr'
require 'colorize'
require 'sqlite3'

class Repository

    def self.insert(db, category, parent_id)
        db.execute "INSERT INTO relations (category, parent) VALUES ('#{category}', #{parent_id.to_i});"
    end

    def self.select(db, category)
        parent_id = -1
        stm = db.prepare "SELECT * FROM relations WHERE category = '#{category}' LIMIT 1;"
        rs = stm.execute 
        if rs != nil
            parent_id = rs.first[0]
        end
        stm.close if stm
        parent_id
    end

    def self.delete(db, category)
        db.execute "DELETE FROM relations WHERE category = '#{category}';"
    end

    def self.print(db)
        stm = db.prepare "SELECT * FROM relations;"
        rs = stm.execute 
        if rs != nil
            rs.each{|item| puts item.join "\s"}
        end
        stm.close if stm
    end
end

class Main

    ATTR_NAME = "user.tags"
    DATABASE_NAME = "tagfile.sqlite3"
    DATABASE_TABLE = "table.sql"

    def add(args)
        file = args.shift
        args.each do|tag| 
            attr = Xattr.new(file)
            tags = attr[ATTR_NAME]
            if tags == nil
                attr.set(ATTR_NAME, " " << tag << " ")
            elsif !tags.include? tag
                tags << tag << " "
                attr[ATTR_NAME] = tags
            end
        end
    end

    def delete(args)
        file = args.shift
        args.each do |tag|
            attr = Xattr.new(file)
            tags = attr[ATTR_NAME]
            if tags != nil
                attr[ATTR_NAME] = tags.gsub(" " + tag + " ", " ")
            end
        end 
    end

    def get(args)
        args.each do |item| 
            attr = Xattr.new(item)
            puts "Tags for #{item}".colorize(:blue)
            puts attr.as_json
        end
    end

    def truncate(args)
        args.each { |item| Xattr.new(item).remove(ATTR_NAME) }
    end

    def search(args)
        items = Dir.entries(".")
        items = items.select { |item| item != '.' && item != '..' }
        items.each do |item|
            if File.file?(item)
                attr = Xattr.new(item)
                tags = attr[ATTR_NAME]
                if tags != nil
                    args.each do |tag| 
                        if tags.include? " " << tag << " "
                            puts item
                        end
                    end
                end
            end
        end
    end
    
    def addgrad(args)
        if args.length == 2
            begin
                db = SQLite3::Database.open DATABASE_NAME
                if args[0] == '_'
                    Repository::insert(db, args[1], 0)
                else
                    parent_id = Repository::select(db, args[0])
                    if parent_id > -1
                        Repository::insert(db, args[1], parent_id)
                    end
                end
                db.close if db
            rescue => e
                puts "addgrag: #{e.message}".colorize(:red)
            end
        else
            puts "Only 2 params!".colorize(:red)
        end
    end

    def deletegrad(args)
        if args.length == 1
            begin
                db = SQLite3::Database.open DATABASE_NAME
                Repository::delete(db, args[0])
                db.close if db
            rescue => e
                puts "deletegrad: #{e.message}".colorize(:red)
            end
        end
    end

    def tree(args)
        begin
            db = SQLite3::Database.open DATABASE_NAME
            Repository::print(db)
            db.close if db
        rescue => e
            puts "print: #{e.message}".colorize(:red)
        end
    end

    def print(args)
        begin
            db = SQLite3::Database.open DATABASE_NAME
            Repository::print(db)
            db.close if db
        rescue => e
            puts "print: #{e.message}".colorize(:red)
        end
    end

    def help(_)
        puts "This is the help method.".colorize(:yellow)
    end

    def init(_)
        if File.file?(DATABASE_NAME)
            puts "Database alredy exists!".colorize(:red)
        else
            db = SQLite3::Database.new DATABASE_NAME
            puts db.execute File.readlines(DATABASE_TABLE).join(' ')
            db.close if db
            puts "Database and table were created!".colorize(:green)
        end
    end
end

if ARGV.length == 0
    return
end

tag_file = Main.new
args = ARGV
command = args.shift

begin
    tag_file.send(command, args)
rescue => error
    puts "send: #{error.message}"
end